package me.flyray.bsin.server.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.facade.service.BsinAdminProcessDefinitionService;
import me.flyray.bsin.server.domain.ProcessDefinition;
import me.flyray.bsin.server.mapper.ActReProcdefMapper;
import me.flyray.bsin.utils.RespBodyHandler;

import org.flowable.ui.modeler.domain.Model;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * 流程定义
 */

public class ProcessDefinitionServiceImpl implements BsinAdminProcessDefinitionService {
    @Autowired
    private ActReProcdefMapper actReProcdefMapper;

    /**
     * 查询最新的流程定义
     *
     * @param requestMap
     * @return
     */
    @Override
    public Map<String, Object> getProcessDefinitionPageList(Map<String, Object> requestMap) {
        Map<String, Object> pagination = (Map<String, Object>) requestMap.get("pagination");
        String tenantId =(String) requestMap.get("tenantId");
        String name =(String) requestMap.get("name");
        String key =(String) requestMap.get("key");
        PageHelper.startPage((Integer) pagination.get("pageNum"), (Integer) pagination.get("pageSize"));
        List<ProcessDefinition> pageList = actReProcdefMapper.getProcessDefinitionPageList(tenantId,name,key);
        PageInfo<ProcessDefinition> pageInfo = new PageInfo<>(pageList);
        return RespBodyHandler.setRespPageInfoBodyDto(pageInfo);

    }

}
