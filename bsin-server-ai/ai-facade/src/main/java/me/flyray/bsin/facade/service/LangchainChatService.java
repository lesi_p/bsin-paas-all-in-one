package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("langchainChatService")
public interface LangchainChatService {

    /**
     * chat
     */
    @POST
    @Path("chat")
    @Produces("application/json")
    public Map<String, Object> chat(Map<String, Object> requestMap);

    /**
     * chatWithDocument
     */
    @POST
    @Path("chatWithDocument")
    @Produces("application/json")
    public Map<String, Object> chatWithDocument(Map<String, Object> requestMap);

}
