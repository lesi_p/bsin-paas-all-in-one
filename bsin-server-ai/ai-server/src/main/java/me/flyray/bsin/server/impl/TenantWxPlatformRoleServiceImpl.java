package me.flyray.bsin.server.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.context.BsinServiceContext;
import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.facade.service.TenantWxPlatformRoleService;
import me.flyray.bsin.server.domain.TenantWxPlatformRole;
import me.flyray.bsin.server.mapper.AiTenantWxPlatformRoleMapper;
import me.flyray.bsin.utils.BsinSnowflake;
import me.flyray.bsin.utils.RespBodyHandler;

/**
* @author bolei
* @description 针对表【ai_tenant_wxmp_role】的数据库操作Service实现
* @createDate 2023-04-25 18:41:30
*/
@Service
public class TenantWxPlatformRoleServiceImpl implements TenantWxPlatformRoleService {


    @Autowired
    private AiTenantWxPlatformRoleMapper tenantWxmpRoleMapper;

    @Override
    public Map<String, Object> add(Map<String, Object> requestMap) {
        TenantWxPlatformRole tenantWxmpRole = BsinServiceContext.getReqBodyDto(TenantWxPlatformRole.class, requestMap);
        TenantWxPlatformRole tenantWxmpRoleResult = tenantWxmpRoleMapper.selectByTenantId(tenantWxmpRole.getTenantId());
        if(tenantWxmpRoleResult != null){
            throw new BusinessException("","");
        }
        tenantWxmpRole.setAppType("mp");
        String serialNo = BsinSnowflake.getId();
        tenantWxmpRole.setSerialNo(serialNo);
        tenantWxmpRoleMapper.insert(tenantWxmpRole);
        return RespBodyHandler.setRespBodyDto(tenantWxmpRole);
    }

    @Override
    public Map<String, Object> delete(Map<String, Object> requestMap) {
        String serialNo = (String)requestMap.get("serialNo");
        // 删除
        tenantWxmpRoleMapper.deleteById(serialNo);
        return RespBodyHandler.RespBodyDto();
    }

    @Override
    public Map<String, Object> edit(Map<String, Object> requestMap) {
        TenantWxPlatformRole tenantWxmpRole = BsinServiceContext.getReqBodyDto(TenantWxPlatformRole.class, requestMap);
        String tenantId = (String)requestMap.get("tenantId");
        TenantWxPlatformRole tenantWxmpRoleResult = tenantWxmpRoleMapper.selectByTenantId(tenantId);
        if(tenantWxmpRoleResult == null){
            throw new BusinessException(ResponseCode.APP_CODE_EXISTS);
        }
        tenantWxmpRole.setSerialNo(tenantWxmpRoleResult.getSerialNo());
        tenantWxmpRoleMapper.updateById(tenantWxmpRole);
        return RespBodyHandler.setRespBodyDto(tenantWxmpRole);
    }

    @Override
    public Map<String, Object> detail(Map<String, Object> requestMap) {
        String tenantId = (String)requestMap.get("tenantId");
        TenantWxPlatformRole tenantWxmpRole = tenantWxmpRoleMapper.selectByTenantId(tenantId);
        return RespBodyHandler.setRespBodyDto(tenantWxmpRole);
    }

}
