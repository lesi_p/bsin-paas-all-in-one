package me.flyray.bsin.server.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @TableName ai_tenant_wxmp_user
 */
@Data
public class TenantWxPlatformUser {

    /**
     *
     */
    private String serialNo;

    /**
     * 微信openid
     */
    private String openId;

    /**
     * 微信应用ID
     */
    private String appId;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 会员名称
     */
    private String name;

    /**
     * 性别
     */
    private String gender;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 城市
     */
    private String city;

    /**
     * 身高
     */
    private Double height;

    /**
     * 生日
     */
    private String birthday;

    /**
     * 学历
     */
    private String education;

    /**
     * 婚姻状况
     */
    private String maritalStatus;

    /**
     * 照片
     */
    private String avatar;

    /**
     * 收入
     */
    private String income;

    /**
     * 星座
     */
    private String constellation;

    /**
     * 性格
     */
    private String character;

    /**
     * 兴趣爱好
     */
    private String interest;

    /**
     * 爱情观
     */
    private String loveView;

    /**
     * 伴侣要求
     */
    private String mateRequirements;

    /**
     * 电话
     */
    private String phone;

    /**
     * 微信号
     */
    private String weixinNo;


    /**
     * token余额
     */
    private Integer tokenBalance;


    /**
     * 使用的token
     */
    private Integer tokenUsed;


    /**
     * 使用的token
     */
    private String chatHistoryRecord;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 状态
     */
    private String status;

}