package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("runtimeService")
public interface BsinRuntimeService {

    /**
     * 根据执行id获取流程变量
     */
    @POST
    @Path("getVariables")
    @Produces("application/json")
    Map<String,Object> getVariables(Map<String, Object> requestMap);

    /**
     * 设置执行任务中的变量
     */
    @POST
    @Path("setVariable")
    @Produces("application/json")
    Map<String,Object> setVariable(Map<String, Object> requestMap);


    /**
     * 根据流程定义Id启动流程实例
     */
    @POST
    @Path("startProcessInstanceByProcessDefinitionId")
    @Produces("application/json")
    Map<String,Object> startProcessInstanceById(Map<String, Object> requestMap);

    /**
     * 根据模型key启动流程实例
     */
    @POST
    @Path("startProcessInstanceByKey")
    @Produces("application/json")
    Map<String,Object> startProcessInstanceByKey(Map<String, Object> requestMap);

    /**
     * 附带表单数据启动流程实例
     */
    @POST
    @Path("startProcessInstanceWithForm")
    @Produces("application/json")
    Map<String,Object> startProcessInstanceWithForm(Map<String, Object> requestMap);

    /**
     * 根据ID查询运行中的流程实例
     *
     * @param
     */
    @POST
    @Path("getProcessInstanceById")
    @Produces("application/json")
    Map<String,Object> getProcessInstanceById(Map<String, Object> requestMap);


    /**
     * 挂起流程实例
     *
     * @param
     */
    @POST
    @Path("suspendProcessInstanceById")
    @Produces("application/json")
    Map<String,Object> suspendProcessInstanceById(Map<String, Object> requestMap);

    /**
     * 查询挂起流程实例
     *
     * @param
     */
    @POST
    @Path("getSuspendedInstances")
    @Produces("application/json")
    Map<String,Object> getSuspendedInstances(Map<String, Object> requestMap);

    /**
     * 激活流程实例
     *
     * @param
     */
    @POST
    @Path("activateProcessInstanceById")
    @Produces("application/json")
    Map<String,Object> activateProcessInstanceById(Map<String, Object> requestMap);

    /**
     * 查询激活流程实例
     *
     * @param
     */
    @POST
    @Path("getActiveInstances")
    @Produces("application/json")
    Map<String,Object> getActiveInstances(Map<String, Object> requestMap);


    /**
     *删除流程实例
     *
     * @param
     */
    @POST
    @Path("deleteProcessInstance")
    @Produces("application/json")
    Map<String,Object> deleteProcessInstance(Map<String, Object> requestMap);
}
