package me.flyray.bsin.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import me.flyray.bsin.server.domain.SysApp;
import me.flyray.bsin.server.domain.SysAppFunction;
import me.flyray.bsin.server.domain.SysDictItem;
import me.flyray.bsin.server.domain.SysProductApp;

/**
* @author bolei
* @description 针对表【sys_product_app】的数据库操作Mapper
* @createDate 2023-11-05 18:00:01
* @Entity generator.domain.SysProductApp
*/

@Repository
@Mapper
public interface ProductAppMapper {

    void insert(SysProductApp appFunction);

    public void deleteById(@Param("productId") String productId, @Param("appId") String appId);

    List<SysApp> selectListByProductId(@Param("productId") String productId);
    
    List<SysApp> selectPageList(@Param("productId") String productId);

    List<String> selectListByProductCode(@Param("productCode") String tenantProductCode);

    SysProductApp selectByProductIdAndAppId(@Param("productId") String productId, @Param("appId") String appId);

}





