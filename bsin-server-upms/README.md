# bsin-server-upms


  **文档版本**

| 版本号 | 修改日期   | 编写   | 修改内容                     | 备注 |
| ------ | ---------- | ------ | ---------------------------- | ---- |
| V1.0.0 | 2022/09/02 | leonard | 新建                         |      |



## 目录结构



## 启动注意事项  
- server.port: 8088

- zookeeper地址修改:  
修改 src/main/resources/application.properties
~~~
# 本地zookeeper
com.alipay.sofa.rpc.registry.address=zookeeper://127.0.0.1:2181
# 生产环境
#com.alipay.sofa.rpc.registry.address=zookeeper://xxx.xxx.xxx.xxx:2181
~~~


