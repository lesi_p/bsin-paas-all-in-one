package me.flyray.bsin.facade.service;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("apps")
public interface AppService {

    /**
     *添加应用
     */
    @POST
    @Path("add")
    @Produces("application/json")
    Map<String,Object> add(Map<String,Object> requestMap);

    /**
     *删除应用
     */
    @POST
    @Path("delete")
    @Produces("application/json")
    Map<String,Object> delete(Map<String,Object> requestMap);

    /**
     *编辑应用
     */
    @POST
    @Path("edit")
    @Produces("application/json")
    Map<String,Object> edit(Map<String,Object> requestMap);

    /**
     *分页查询
     */
    @POST
    @Path("getPageList")
    @Produces("application/json")
    Map<String,Object> getPageList(Map<String,Object> requestMap);

    /**
     *根据当前租户查询应用
     */
    @POST
    @Path("getAuthorizableList")
    @Produces("application/json")
    Map<String,Object> getAuthorizableList(Map<String,Object> requestMap);

    /**
     *根据租户id查询应用
     */
    @POST
    @Path("getAuthorizedListByTenantId")
    @Produces("application/json")
    Map<String,Object> getAuthorizedList(Map<String,Object> requestMap);


    /**
     *查询所有已发布的应用
     */
    @POST
    @Path("getPublishedApps")
    @Produces("application/json")
    Map<String,Object> getPublishedApps(Map<String,Object> requestMap);

    /**
     * 添加应用功能
     */
    @POST
    @Path("addAppFunction")
    @Produces("application/json")
    public Map<String, Object> addAppFunction(Map<String, Object> requestMap);

    /**
     * 删除应用功能
     */
    @POST
    @Path("deleteAppFunction")
    @Produces("application/json")
    public Map<String, Object> deleteAppFunction(Map<String, Object> requestMap);

    /**
     * 查询应用功能列表
     */
    @POST
    @Path("getAppFunctionList")
    @Produces("application/json")
    public Map<String, Object> getAppFunctionList(Map<String, Object> requestMap);

    /**
     * 查询应用功能列表
     */
    @POST
    @Path("getAppFunctionPageList")
    @Produces("application/json")
    public Map<String, Object> getAppFunctionPageList(Map<String, Object> requestMap);

}
